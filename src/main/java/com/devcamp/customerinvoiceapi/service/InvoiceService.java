package com.devcamp.customerinvoiceapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.model.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;
    Invoice invoice1 = new Invoice(1, 1000);
    Invoice invoice2 = new Invoice(2, 2000);
    Invoice invoice3 = new Invoice(3, 3000);
    Invoice invoice4 = new Invoice(4, 6000);
    Invoice invoice5 = new Invoice(5, 7000);
    Invoice invoice6 = new Invoice(6, 8000);

    public ArrayList<Invoice> allInvoices() {
        ArrayList<Invoice> invoices = new ArrayList<>();
        invoice1.setCustomer(customerService.customer1);
        invoice2.setCustomer(customerService.customer2);
        invoice3.setCustomer(customerService.customer3);
        invoice4.setCustomer(customerService.customer2);

        invoice5.setCustomer(customerService.customer1);
        invoice6.setCustomer(customerService.customer3);

        invoices.add(invoice1);
        invoices.add(invoice2);
        invoices.add(invoice3);
        invoices.add(invoice2);
        invoices.add(invoice1);
        invoices.add(invoice3);

        return invoices;

    }

    public Invoice invoiceIndex(int paramIndex) {
        ArrayList<Invoice> invoices = allInvoices();
        Invoice invoice = new Invoice();
        if (paramIndex < -1 || paramIndex > 5) {
            return invoice;
        } else {
            invoice = invoices.get(paramIndex);
        }
        return invoice;
    }

}
